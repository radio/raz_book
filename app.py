from flask import Flask,render_template,request,Response,make_response
import os

import utils

app = Flask(__name__)
app.jinja_env.auto_reload = True
app.config['TEMPLATES_AUTO_RELOAD'] = True


conf = utils.get_yaml_data(os.path.dirname(os.path.abspath(__file__)) + '/conf/config.yml')
res_dir = conf['res_dir']

@app.route('/')
def hello_world():
    return render_template('index.html')


@app.route('/goto/levels')
def goto_levels_index():
    """
    进入书籍的level列表页面
    :return:
    """
    book = request.args.get('book')
    # 读取目录
    data = {'book_title':book}
    book_dir = res_dir+'/'+book
    level_names = [level for level in os.listdir(book_dir) if os.path.isdir(book_dir+'/'+level) and not level.startswith('@')]
    levels = sorted(level_names)
    if 'aa' in levels:
        levels.remove('aa')
        levels.insert(0,'aa')
    data['levels'] = levels
    return render_template('levels.html', **data)


@app.route('/goto/book/level')
def goto_book_index():
    """
    进入书籍的某个level
    :return:
    """
    book = request.args.get('book')
    level = request.args.get('level')
    # 读取目录
    data = {'book_title': book, 'level':level}
    level_dir = res_dir+'/'+book+'/'+level
    lesson_names = sorted([lesson for lesson in os.listdir(level_dir) if os.path.isdir(level_dir+'/'+lesson) and not lesson.startswith('@')])
    lessons = []
    for lesson in lesson_names:
        lesson_data = {'name': lesson}
        files = [each for each in os.listdir(level_dir+'/'+lesson) if os.path.isfile(level_dir+'/'+lesson+'/'+each) and each[-3:] in ['jpg','png']]
        if files:
            lesson_data['cover'] = sorted(files)[0]
        lessons.append(lesson_data)
    data['lessons'] = lessons
    return render_template('lessons.html', **data)


@app.route('/goto/play')
def goto_play_lesson():
    book = request.args.get('book')
    level = request.args.get('level')
    lesson = request.args.get('lesson')

    lesson_dir = res_dir + '/' + book + '/' + level + '/' + lesson
    images = sorted([each for each in os.listdir(lesson_dir) if
             os.path.isfile(lesson_dir + '/' + each) and each[-3:] in ['jpg', 'png']],key=lambda x: int(x[:-4].split('-')[1]))
    if book == 'RAZ':
        result = check_raz_mp3(lesson_dir, images)
    data = {'book_title': book, 'level': level, 'lesson': lesson, 'items': result}
    return render_template('lesson.html', **data)



def check_raz_mp3(base_dir, images):
    """
    以RAZ的资源规范，检查每个页面是否有音频
    :return:
    {resources:[{img:'aaa.jpg', mp3:'aaa.mp3'},title_mp3:'xxx.mp3']}
    如：
    {'title_mp3': 'raz_out_title_text.mp3', 'resources': [{'img': 'page-1.jpg'}, {'img': 'page-2.jpg'}, {'img': 'page-3.jpg', 'mp3': 'raz_out_p3_text.mp3'}, {'img': 'page-4.jpg', 'mp3': 'raz_out_p4_text.mp3'}, {'img': 'page-5.jpg', 'mp3': 'raz_out_p5_text.mp3'}, {'img': 'page-6.jpg', 'mp3': 'raz_out_p6_text.mp3'}, {'img': 'page-7.jpg', 'mp3': 'raz_out_p7_text.mp3'}, {'img': 'page-8.jpg', 'mp3': 'raz_out_p8_text.mp3'}, {'img': 'page-9.jpg', 'mp3': 'raz_out_p9_text.mp3'}, {'img': 'page-10.jpg', 'mp3': 'raz_out_p10_text.mp3'}, {'img': 'page-11.jpg'}]}
    """
    result = {}
    mp3_list = sorted([each for each in os.listdir(base_dir) if os.path.isfile(base_dir + '/' + each) and each[-3:] == 'mp3'])
    # 找封面读音
    for each in reversed(mp3_list):
        if each.endswith('title_text.mp3'):
            result['title_mp3'] = each
            break
    # 找每个图片对应的读音
    img_mp3 = []
    for img_file in images:
        num = img_file[:-4].split('-')[1] # 数字
        item = {'img': img_file}
        for mp3_file in mp3_list:
            if mp3_file.endswith('p'+num+'_text.mp3'):
                item['mp3'] = mp3_file
                break
        img_mp3.append(item)
    result['resources'] = img_mp3
    return result


#http://127.0.0.1:8080/favicon.ico

@app.route('/favicon.ico')
def get_favicon():
    with open('./static/icons/books.png', 'rb') as f:
        image = f.read()
    resp = Response(image, mimetype="image/png")
    return resp


@app.route('/get_img')
def get_img():
    """
    返回图片流
    :return:
    """
    book = request.args.get('book')
    level = request.args.get('level')
    lesson = request.args.get('lesson')
    file_name = request.args.get('file')
    img_path = "{}/{}/{}/{}/{}".format(res_dir, book, level, lesson, file_name)
    with open(img_path, 'rb') as f:
        image = f.read()
    resp = Response(image, mimetype="image/jpeg")
    return resp


@app.route('/get_audio.mp3')
def get_audio():
    """
    返回音频流
    :return:
    """
    book = request.args.get('book')
    level = request.args.get('level')
    lesson = request.args.get('lesson')
    file_name = request.args.get('file')
    mp3_path = "{}/{}/{}/{}/{}".format(res_dir, book, level, lesson, file_name)
    with open(mp3_path, 'rb') as f:
        mp3 = f.read()
    resp = Response(mp3, mimetype="audio/mp3")
    return resp

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080,debug=False)
