#!/usr/bin/env python
# -*- coding:utf-8 -*-
# @FileName  :utils.py
# @Time      :2020/11/13 下午3:54
# @Author    :sam
# @Desc      :

import yaml
import os


def get_yaml_data(yaml_file):
    # 打开yaml文件
    file = open(yaml_file, 'r', encoding="utf-8")
    file_data = file.read()
    file.close()
    # 将字符串转化
    return yaml.safe_load(file_data)

# data = get_yaml_data('/home/tree/workspace/pythonworkspace/book/conf/config.yml')
# print(data)